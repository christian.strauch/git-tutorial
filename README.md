# git-tutorial
This is a short README to our git workshop.

## Getting started
- go on `git.rwth-aachen.de`. You will see my project
- Press `Fork` to create a fork of the project
- use the Windows Explorer and go in a suitable directory
- create a local directory named `git`
- open a terminal in this directory
- `git config --global user.name <yourusername>, for example "christian.strauch"
- `git config --global user.mail <youremail>`
- go in gitlab in the prererences of your account and then to access token. Generate an access token for all rights and save the password in a local file.

- `git clone https://git.rwth-aachen.de/<your-username>/git-tutorial.git>>
- you will asked for username and password. Username is your email and password is the password of the access token
-git upstream https://git.rwth-aachen.de/christian.strauch/git-tutorial.git`
- check with `git remote -v` if you have an origin stream (your fork) and a stream called `upstream` (my repository)


## Basic commands
- `git status`: This command shows the status of your repository, including which files have been modified but not yet added or committed.
- `git diff`: This command shows the differences between the version of the files in your repository and the version of the files currently on your filesystem.
- `git log`: This command shows a history of all the commits made to your repository.
- `git add <file>`: This command stages the specified file, adding it to the list of files that will be included in the next commit.
- `git commit -m "<message>"`: This command commits the staged changes to your repository with the specified commit message.
- `git push`: This command pushes the local commits in your repository to a remote repository, such as a repository hosted on GitHub.
- `git pull`: This command pulls the latest commits in the remote repository to your local repository

## Commands for working with different branches
- `git branch`: This command lists all of the branches in your repository.
- `git branch <branch>`: This command creates a new branch with the specified name.
- `git branch -d <branch>`: This command deletes the specified branch.
- `git checkout <branch>`: This command switches to the specified branch.
- `git merge <branch>`: This command merges the specified branch into the current branch.
- `git branch -m <new_name>`: This command renames the current branch.

## Create tags
- `git tag -a v1.4 -m 'version 1.4'`: This command defines a new version 1.4
- `git push origin <tag_name>`: This commands pushes the tag to the remote repository (example: `git push origin v1.4`)

## Working with forks
- `git clone <repository>`: This command creates a local copy of a remote repository.
- `git fork <upstream_repository>`: This command creates a fork of the specified upstream repository on your GitHub account.
- `git pull <upstream_repository> <branch>`: This command pulls changes from the specified upstream repository and branch into your local repository.
- `git push <remote> <branch>`: This command pushes the specified branch to the specified remote repository. example: `git push fork main`





